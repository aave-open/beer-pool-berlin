const etherlime = require('etherlime');
const BeerPoolContract = require('../build/BeerPoolContract.json');

const deploy = async (network, secret) => {

	const deployer = new etherlime.EtherlimeGanacheDeployer();
	const contract = await deployer.deploy(BeerPoolContract);
};

module.exports = {
	deploy
};
