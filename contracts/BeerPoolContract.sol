pragma solidity ^0.5.0;

import "tabookey-gasless/contracts/RelayRecipient.sol";
import "@openzeppelin/contracts/ownership/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/SafeERC20.sol";
import "./IndexedMerkleProof.sol";


contract BeerPoolContract is Ownable, RelayRecipient {
    using IndexedMerkleProof for bytes;
    using SafeERC20 for IERC20;

    IERC20 dai = IERC20(0x89d24A6b4CcB1B6fAA2625fE562bDD9a23260359);
    uint160 public merkleRoot;
    uint256 public amountPerUser;
    uint256 public stakingDuration;
    mapping(address => uint256) public stakedAt;
    uint256[1000000] public redeemBitMask;

    event Staked(address who);
    event Redeemed(address who, address where);
    event Aborted();

    constructor(uint160 root, uint256 amount, uint256 duration) public {
        merkleRoot = root;
        amountPerUser = amount;
        stakingDuration = duration;
        setRelayHub(IRelayHub(0xD216153c06E857cD7f72665E0aF1d7D82172F494));
    }

    function acceptRelayedCall(
        address /*relay*/,
        address from,
        bytes memory encodedFunction,
        uint256 /*transactionFee*/,
        uint256 /*gasPrice*/,
        uint256 /*gasLimit*/,
        uint256 /*nonce*/,
        bytes memory /*approvalData*/,
        uint256 /*maxPossibleCharge*/
    )
        public
        view
        returns (uint256, bytes memory)
    {
        // "Stack to deep" fix
        address sender = from;

        uint256 offset;
        bytes memory merkleProof;
        assembly {
            offset := mload(add(encodedFunction, 36)) // 32 + 4
            merkleProof := add(encodedFunction, add(36, offset)) // 32 + 4 + offset
        }

        if (encodedFunction[0] == this.stake.selector[0] &&
            encodedFunction[1] == this.stake.selector[1] &&
            encodedFunction[2] == this.stake.selector[2] &&
            encodedFunction[3] == this.stake.selector[3])
        {
            (uint160 root,) = merkleProof.compute(uint160(uint256(keccak256(abi.encodePacked(sender)))));
            if (root == merkleRoot && !wasStaked(sender)) {
                return (0, "");
            }
        }

        if (encodedFunction[0] == this.redeem.selector[0] &&
            encodedFunction[1] == this.redeem.selector[1] &&
            encodedFunction[2] == this.redeem.selector[2] &&
            encodedFunction[3] == this.redeem.selector[3])
        {
            (uint160 root, uint256 index) = merkleProof.compute(uint160(uint256(keccak256(abi.encodePacked(sender)))));
            if (root == merkleRoot && wasStaked(sender) && now >= stakedAt[sender] + stakingDuration && !wasRedeemed(index)) {
                return (0, "");
            }
        }

        return (777, "Error: 777");
    }

    function wasStaked(address wallet) public view returns(bool) {
        return stakedAt[wallet] != 0;
    }

    function wasRedeemed(uint index) public view returns(bool) {
        return redeemBitMask[index / 256] & (1 << (index % 256)) != 0;
    }

    function wasRedeemedByWalletAndProof(address wallet, bytes memory merkleProof) public view returns(bool) {
        (uint160 root, uint256 index) = merkleProof.compute(uint160(uint256(keccak256(abi.encodePacked(wallet)))));
        require(root == merkleRoot, "Merkle root doesn't match");
        return wasRedeemed(index);
    }

    function stake(bytes memory merkleProof) public {
        (uint160 root,) = merkleProof.compute(uint160(uint256(keccak256(abi.encodePacked(getSender())))));
        require(root == merkleRoot);
        require(!wasStaked(getSender()));

        stakedAt[getSender()] = now;
        emit Staked(getSender());
    }

    function redeem(bytes memory merkleProof, address receiver) public {
        (uint160 root, uint256 index) = merkleProof.compute(uint160(uint256(keccak256(abi.encodePacked(getSender())))));
        require(root == merkleRoot);
        require(wasStaked(getSender()) && now >= stakedAt[getSender()] + stakingDuration);
        require(!wasRedeemed(index));

        redeemBitMask[index / 256] |= (1 << (index % 256));
        dai.safeTransfer(receiver, amountPerUser);
        emit Redeemed(getSender(), receiver);
    }

    function abort() public onlyOwner {
        IRelayHub hub = IRelayHub(getHubAddr());
        hub.withdraw(hub.balanceOf(address(this)), msg.sender);
        dai.safeTransfer(msg.sender, dai.balanceOf(address(this)));
        emit Aborted();
    }

    function preRelayedCall(bytes calldata context) external returns (bytes32) {
    }

    function postRelayedCall(bytes calldata context, bool success, uint actualCharge, bytes32 preRetVal) external {
    }
}
