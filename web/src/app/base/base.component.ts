import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss']
})
export class BaseComponent implements OnInit {

  loading = true;

  constructor() {
  }

  ngOnInit() {

    this.loading = false;
  }

}
