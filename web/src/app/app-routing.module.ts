import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BaseComponent} from './base/base.component';
import {NoContentComponent} from './no-content/no-content.component';

const routes: Routes = [
    {
        path: '',
        component: BaseComponent,
        children: [
            {
                path: '',
                loadChildren: './beer-pool/beer-pool.module#BeerPoolModule'
            },
            {
                path: ':data',
                loadChildren: './beer-pool/beer-pool.module#BeerPoolModule'
            }
        ]
    },
    {
        path: '**',
        component: NoContentComponent
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            routes,
            {
                enableTracing: false,
                useHash: true
            }
        )
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
