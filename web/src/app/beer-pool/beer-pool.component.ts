import {Component, OnInit} from '@angular/core';
import {Observable, timer} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {MerkleTree} from './merkle-tree';
import Web3 from 'web3';
import {ethers} from 'ethers';
import {HttpClient} from '@angular/common/http';

declare let require: any;

const tabookey = require('tabookey-gasless');

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

@Component({
    selector: 'app-beer-pool',
    templateUrl: './beer-pool.component.html',
    styleUrls: ['./beer-pool.component.scss']
})
export class BeerPoolComponent implements OnInit {

    contractABI = [
        {
            'constant': true,
            'inputs': [
                {
                    'internalType': 'address',
                    'name': 'origSender',
                    'type': 'address'
                },
                {
                    'internalType': 'bytes',
                    'name': 'msgData',
                    'type': 'bytes'
                }
            ],
            'name': 'getSenderFromData',
            'outputs': [
                {
                    'internalType': 'address',
                    'name': '',
                    'type': 'address'
                }
            ],
            'payable': false,
            'stateMutability': 'view',
            'type': 'function'
        },
        {
            'constant': true,
            'inputs': [],
            'name': 'merkleRoot',
            'outputs': [
                {
                    'internalType': 'uint160',
                    'name': '',
                    'type': 'uint160'
                }
            ],
            'payable': false,
            'stateMutability': 'view',
            'type': 'function'
        },
        {
            'constant': true,
            'inputs': [],
            'name': 'getSender',
            'outputs': [
                {
                    'internalType': 'address',
                    'name': '',
                    'type': 'address'
                }
            ],
            'payable': false,
            'stateMutability': 'view',
            'type': 'function'
        },
        {
            'constant': false,
            'inputs': [],
            'name': 'renounceOwnership',
            'outputs': [],
            'payable': false,
            'stateMutability': 'nonpayable',
            'type': 'function'
        },
        {
            'constant': true,
            'inputs': [],
            'name': 'getHubAddr',
            'outputs': [
                {
                    'internalType': 'address',
                    'name': '',
                    'type': 'address'
                }
            ],
            'payable': false,
            'stateMutability': 'view',
            'type': 'function'
        },
        {
            'constant': true,
            'inputs': [],
            'name': 'amountPerUser',
            'outputs': [
                {
                    'internalType': 'uint256',
                    'name': '',
                    'type': 'uint256'
                }
            ],
            'payable': false,
            'stateMutability': 'view',
            'type': 'function'
        },
        {
            'constant': true,
            'inputs': [],
            'name': 'stakingDuration',
            'outputs': [
                {
                    'internalType': 'uint256',
                    'name': '',
                    'type': 'uint256'
                }
            ],
            'payable': false,
            'stateMutability': 'view',
            'type': 'function'
        },
        {
            'constant': true,
            'inputs': [],
            'name': 'owner',
            'outputs': [
                {
                    'internalType': 'address',
                    'name': '',
                    'type': 'address'
                }
            ],
            'payable': false,
            'stateMutability': 'view',
            'type': 'function'
        },
        {
            'constant': true,
            'inputs': [],
            'name': 'isOwner',
            'outputs': [
                {
                    'internalType': 'bool',
                    'name': '',
                    'type': 'bool'
                }
            ],
            'payable': false,
            'stateMutability': 'view',
            'type': 'function'
        },
        {
            'constant': true,
            'inputs': [
                {
                    'internalType': 'uint256',
                    'name': '',
                    'type': 'uint256'
                }
            ],
            'name': 'redeemBitMask',
            'outputs': [
                {
                    'internalType': 'uint256',
                    'name': '',
                    'type': 'uint256'
                }
            ],
            'payable': false,
            'stateMutability': 'view',
            'type': 'function'
        },
        {
            'constant': true,
            'inputs': [],
            'name': 'getRecipientBalance',
            'outputs': [
                {
                    'internalType': 'uint256',
                    'name': '',
                    'type': 'uint256'
                }
            ],
            'payable': false,
            'stateMutability': 'view',
            'type': 'function'
        },
        {
            'constant': true,
            'inputs': [],
            'name': 'getMessageData',
            'outputs': [
                {
                    'internalType': 'bytes',
                    'name': '',
                    'type': 'bytes'
                }
            ],
            'payable': false,
            'stateMutability': 'view',
            'type': 'function'
        },
        {
            'constant': true,
            'inputs': [
                {
                    'internalType': 'address',
                    'name': '',
                    'type': 'address'
                }
            ],
            'name': 'stakedAt',
            'outputs': [
                {
                    'internalType': 'uint256',
                    'name': '',
                    'type': 'uint256'
                }
            ],
            'payable': false,
            'stateMutability': 'view',
            'type': 'function'
        },
        {
            'constant': false,
            'inputs': [
                {
                    'internalType': 'address',
                    'name': 'newOwner',
                    'type': 'address'
                }
            ],
            'name': 'transferOwnership',
            'outputs': [],
            'payable': false,
            'stateMutability': 'nonpayable',
            'type': 'function'
        },
        {
            'inputs': [
                {
                    'internalType': 'uint160',
                    'name': 'root',
                    'type': 'uint160'
                },
                {
                    'internalType': 'uint256',
                    'name': 'amount',
                    'type': 'uint256'
                },
                {
                    'internalType': 'uint256',
                    'name': 'duration',
                    'type': 'uint256'
                }
            ],
            'payable': false,
            'stateMutability': 'nonpayable',
            'type': 'constructor'
        },
        {
            'anonymous': false,
            'inputs': [
                {
                    'indexed': true,
                    'internalType': 'address',
                    'name': 'previousOwner',
                    'type': 'address'
                },
                {
                    'indexed': true,
                    'internalType': 'address',
                    'name': 'newOwner',
                    'type': 'address'
                }
            ],
            'name': 'OwnershipTransferred',
            'type': 'event'
        },
        {
            'constant': true,
            'inputs': [
                {
                    'internalType': 'address',
                    'name': '',
                    'type': 'address'
                },
                {
                    'internalType': 'address',
                    'name': 'from',
                    'type': 'address'
                },
                {
                    'internalType': 'bytes',
                    'name': 'encodedFunction',
                    'type': 'bytes'
                },
                {
                    'internalType': 'uint256',
                    'name': '',
                    'type': 'uint256'
                },
                {
                    'internalType': 'uint256',
                    'name': '',
                    'type': 'uint256'
                },
                {
                    'internalType': 'uint256',
                    'name': '',
                    'type': 'uint256'
                },
                {
                    'internalType': 'uint256',
                    'name': '',
                    'type': 'uint256'
                },
                {
                    'internalType': 'bytes',
                    'name': '',
                    'type': 'bytes'
                },
                {
                    'internalType': 'uint256',
                    'name': '',
                    'type': 'uint256'
                }
            ],
            'name': 'acceptRelayedCall',
            'outputs': [
                {
                    'internalType': 'uint256',
                    'name': '',
                    'type': 'uint256'
                },
                {
                    'internalType': 'bytes',
                    'name': '',
                    'type': 'bytes'
                }
            ],
            'payable': false,
            'stateMutability': 'view',
            'type': 'function'
        },
        {
            'constant': true,
            'inputs': [
                {
                    'internalType': 'address',
                    'name': 'wallet',
                    'type': 'address'
                }
            ],
            'name': 'wasStaked',
            'outputs': [
                {
                    'internalType': 'bool',
                    'name': '',
                    'type': 'bool'
                }
            ],
            'payable': false,
            'stateMutability': 'view',
            'type': 'function'
        },
        {
            'constant': true,
            'inputs': [
                {
                    'internalType': 'uint256',
                    'name': 'index',
                    'type': 'uint256'
                }
            ],
            'name': 'wasRedeemed',
            'outputs': [
                {
                    'internalType': 'bool',
                    'name': '',
                    'type': 'bool'
                }
            ],
            'payable': false,
            'stateMutability': 'view',
            'type': 'function'
        },
        {
            'constant': true,
            'inputs': [
                {
                    'internalType': 'address',
                    'name': 'wallet',
                    'type': 'address'
                },
                {
                    'internalType': 'bytes',
                    'name': 'merkleProof',
                    'type': 'bytes'
                }
            ],
            'name': 'wasRedeemedByWalletAndProof',
            'outputs': [
                {
                    'internalType': 'bool',
                    'name': '',
                    'type': 'bool'
                }
            ],
            'payable': false,
            'stateMutability': 'view',
            'type': 'function'
        },
        {
            'constant': false,
            'inputs': [
                {
                    'internalType': 'bytes',
                    'name': 'merkleProof',
                    'type': 'bytes'
                }
            ],
            'name': 'stake',
            'outputs': [],
            'payable': false,
            'stateMutability': 'nonpayable',
            'type': 'function'
        },
        {
            'constant': false,
            'inputs': [
                {
                    'internalType': 'bytes',
                    'name': 'merkleProof',
                    'type': 'bytes'
                },
                {
                    'internalType': 'address',
                    'name': 'receiver',
                    'type': 'address'
                }
            ],
            'name': 'redeem',
            'outputs': [],
            'payable': false,
            'stateMutability': 'nonpayable',
            'type': 'function'
        },
        {
            'constant': false,
            'inputs': [
                {
                    'internalType': 'contract IERC20',
                    'name': 'token',
                    'type': 'address'
                }
            ],
            'name': 'abortAndRetrieve',
            'outputs': [],
            'payable': false,
            'stateMutability': 'nonpayable',
            'type': 'function'
        },
        {
            'constant': false,
            'inputs': [
                {
                    'internalType': 'bytes',
                    'name': 'context',
                    'type': 'bytes'
                }
            ],
            'name': 'preRelayedCall',
            'outputs': [
                {
                    'internalType': 'bytes32',
                    'name': '',
                    'type': 'bytes32'
                }
            ],
            'payable': false,
            'stateMutability': 'nonpayable',
            'type': 'function'
        },
        {
            'constant': false,
            'inputs': [
                {
                    'internalType': 'bytes',
                    'name': 'context',
                    'type': 'bytes'
                },
                {
                    'internalType': 'bool',
                    'name': 'success',
                    'type': 'bool'
                },
                {
                    'internalType': 'uint256',
                    'name': 'actualCharge',
                    'type': 'uint256'
                },
                {
                    'internalType': 'bytes32',
                    'name': 'preRetVal',
                    'type': 'bytes32'
                }
            ],
            'name': 'postRelayedCall',
            'outputs': [],
            'payable': false,
            'stateMutability': 'nonpayable',
            'type': 'function'
        }
    ];
    contractAddress = '0x32D8fc38839398b5e097215d4D5F3b09df497d61';

    public gasPriceUrl = 'https://gasprice.poa.network';

    loaded = false;
    stakingFinished = false;
    staked = false;
    redeemed = false;
    stakingClicked = false;
    redeemClicked = false;

    remainingTime;
    everySecond: Observable<number> = timer(0, 1000);
    privateKey;
    root;
    index;
    account;
    web3;
    receiver;
    proof;
    contract;
    stakedAt;
    stakingDuration;

    fastGasPrice;
    standardGasPrice;
    instantGasPrice;
    data;
    showInfo = false;

    constructor(
        private route: ActivatedRoute,
        private http: HttpClient
    ) {
    }

    async ngOnInit() {

        this.processParams();
    }

    async getGasPrices() {

        const result = await this.http.get(this.gasPriceUrl).toPromise();

        this.fastGasPrice = new ethers.utils.BigNumber(Math.trunc(result['fast'] * 100)).mul(1e7);
        this.standardGasPrice = new ethers.utils.BigNumber(Math.trunc(result['standard'] * 100)).mul(1e7);
        this.instantGasPrice = new ethers.utils.BigNumber(Math.trunc(result['instant'] * 100)).mul(1e7);
    }

    async processParams() {

        try {

            await this.getGasPrices();

            if (this.route.snapshot.paramMap.get('data')) {

                this.data = this.route.snapshot.paramMap.get('data');
                localStorage.setItem('data', this.data);

            } else if (localStorage.getItem('data')) {

                this.data = localStorage.getItem('data');
            }

            if (this.data) {

                this.privateKey = this.data.split('-')[0];
                this.proof = this.data.split('-').slice(1);

                // console.log('proof', this.proof);

                const web3 = new Web3('https://parity.1inch.exchange/rpc');

                this.account = web3.eth.accounts
                    .privateKeyToAccount(this.privateKey);

                const {root, index} = MerkleTree.applyProof(
                    this.account.address,
                    this.proof
                );

                this.root = root;
                this.index = index;

                // console.log('web3.currentProvider', web3.currentProvider);

                web3.eth.accounts.wallet.add(this.account);
                web3.eth.defaultAccount = this.account.address;

                const provider = new tabookey.RelayProvider(web3.currentProvider, {
                    force_gasprice: this.fastGasPrice.toString(),
                    verbose: true
                });

                provider.relayClient.web3.eth.accounts.wallet.add(this.account);
                provider.relayClient.web3.eth.defaultAccount = this.account.address;

                this.web3 = new Web3(provider);

                this.web3.eth.accounts.wallet.add(this.account);
                this.web3.eth.defaultAccount = this.account.address;

                this.contract = new this.web3.eth.Contract(
                    this.contractABI,
                    this.contractAddress,
                    {
                        gasPrice: this.fastGasPrice.toString()
                    }
                );

                // console.log('this.contract', this.contract);

                const [
                    staked, redeemed, stakedAt, stakingDuration
                ] = await Promise.all([
                    this.contract.methods.wasStaked(this.account.address).call(),
                    this.contract.methods.wasRedeemedByWalletAndProof(
                        this.account.address,
                        '0x' + this.proof.map(value => value.substr(2)).join('')
                    ).call(),
                    this.contract.methods.stakedAt(this.account.address).call(),
                    this.contract.methods.stakingDuration().call(),
                ]);

                this.loaded = true;
                this.staked = staked;
                this.redeemed = redeemed;
                this.stakedAt = stakedAt;
                this.stakingDuration = stakingDuration;
                this.remainingTime = parseInt(this.stakedAt) * 1000 + 900 * 1000 - +new Date();

                if (this.stakedAt != 0 && this.remainingTime < 0) {
                    this.stakingFinished = true;
                }

                // console.log('this.staked', this.staked);
                // console.log('this.redeemed', this.redeemed);
                // console.log('this.stakedAt', this.stakedAt);
                // console.log('this.remainingTime', this.remainingTime);
                // console.log('this.stakingFinished', this.stakingFinished);

                this.everySecond.subscribe(async (second) => {

                    if (this.stakedAt == 0 && second % 5 == 0) {
                        this.contract.methods.stakedAt(this.account.address).call().then(_stakedAt => {
                            // console.log('stakedAt', _stakedAt);
                            this.stakedAt = _stakedAt;
                        });
                    }

                    if (this.stakedAt == 0) {
                        this.remainingTime = 900000;
                        return;
                    }

                    // tslint:disable-next-line:radix
                    this.remainingTime = parseInt(this.stakedAt) * 1000 + 930 * 1000 - +new Date();

                    if (this.remainingTime < 0) {

                        this.stakingFinished = true;

                        delete this.everySecond;
                    }
                });
            } else {

                this.showInfo = true;
            }
        } catch (e) {

            console.error(e);
        }
    }

    async stake() {

        if (this.stakingClicked) {

            return;
        }

        try {

            this.stakingClicked = true;

            const tx = await this.contract.methods.stake(
                '0x' + this.proof.map(value => value.substr(2)).join('')
            ).send({
                from: this.account.address,
                gasPrice: this.fastGasPrice.toString(),
                gas: 100000
            });

            console.log('tx', tx);

        } catch (e) {

            // alert(e);
            console.error(e);
            await sleep(30000);
        }

        this.staked = true;
    }

    async redeem() {

        try {

            this.redeemClicked = true;

            const tx = await this.contract.methods.redeem(
                '0x' + this.proof.map(value => value.substr(2)).join(''),
                this.receiver
            ).send({
                from: this.account.address,
                gasPrice: this.fastGasPrice.toString(),
                gas: 200000
            });

            console.log('tx', tx);

        } catch (e) {

            // alert(e);
            console.error(e);
            await sleep(30000);
        }

        this.redeemed = true;
    }
}
