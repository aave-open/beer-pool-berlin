import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {BeerPoolComponent} from './beer-pool.component';

const routes: Routes = [
  {
    path: '',
    component: BeerPoolComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    FormsModule,
  ],
  exports: [RouterModule]
})
export class BeerPoolRoutingModule {
}
