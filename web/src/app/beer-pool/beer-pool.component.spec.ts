import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeerPoolComponent } from './beer-pool.component';

describe('BeerPoolComponent', () => {
  let component: BeerPoolComponent;
  let fixture: ComponentFixture<BeerPoolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeerPoolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeerPoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
