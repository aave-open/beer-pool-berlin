import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BeerPoolComponent} from './beer-pool.component';
import {LoadingSpinnerModule} from '../loading-spinner/loading-spinner.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BeerPoolRoutingModule} from './beer-pool-routing.module';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
    declarations: [BeerPoolComponent],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        LoadingSpinnerModule,
        BeerPoolRoutingModule,
        HttpClientModule
    ]
})
export class BeerPoolModule {
}
