"use strict";
exports.__esModule = true;
var _a = require('ethereumjs-util'), keccak256 = _a.keccak256, bufferToHex = _a.bufferToHex;
function keccak160(input) {
    return keccak256(input).slice(12);
}
exports.keccak160 = keccak160;
;
var MerkleTree = /** @class */ (function () {
    function MerkleTree(elements) {
        // Create layers
        this.layers = null;
        this.elements = null;
        this.elements = elements;
        this.layers = this.getLayers(elements.map(function (el) { return keccak160(el); }));
        return this;
    }
    MerkleTree.applyProof = function (account, proof) {
        var index = 0;
        console.log('Account0', account);
        account = keccak160(account);
        console.log('keccak160(Account0)', account);
        console.log('Proof1', proof);
        proof = proof.map(function (el) { return new Buffer(el, 'hex'); });
        console.log('Proof2', proof);
        for (var i = 0; i < proof.length; i++) {
            if (Buffer.compare(account, proof[i]) < 0) {
                account = keccak160(Buffer.concat([account, proof[i]]));
                index += 1 << i;
            }
            else {
                account = keccak160(Buffer.concat([proof[i], account]));
            }
        }
        return {
            root: account,
            index: index
        };
    };
    MerkleTree.prototype.getLayers = function (elements) {
        var emptyLeveled = keccak160('');
        if ((elements.length % 2) === 1) {
            elements.push(emptyLeveled);
        }
        var tree = [elements];
        var maxLevel = Math.log2(elements.length);
        for (var level = 1; level <= maxLevel; level++) {
            var current = [];
            for (var i = 0; i < tree[level - 1].length / 2; i++) {
                var a = tree[level - 1][i * 2];
                var b = tree[level - 1][i * 2 + 1];
                var hash = void 0;
                if (Buffer.compare(a, b) < 0) {
                    hash = keccak160(Buffer.concat([a, b]));
                }
                else {
                    hash = keccak160(Buffer.concat([b, a]));
                }
                current.push(hash);
            }
            if (current.length & 1 && level < maxLevel) {
                current.push(emptyLeveled);
            }
            emptyLeveled = keccak160(Buffer.concat([emptyLeveled, emptyLeveled]));
            tree.push(current);
        }
        return tree;
    };
    MerkleTree.prototype.getRoot = function () {
        return this.layers[this.layers.length - 1][0];
    };
    MerkleTree.prototype.getHexRoot = function () {
        return bufferToHex(this.getRoot());
    };
    MerkleTree.prototype.getProof = function (idx) {
        var _this = this;
        if (idx === -1) {
            throw new Error('Element does not exist in Merkle tree');
        }
        return this.layers.reduce(function (proof, layer) {
            var pairElement = _this.getPairElement(idx, layer);
            if (pairElement) {
                proof.push(pairElement);
            }
            idx = Math.floor(idx / 2);
            return proof;
        }, []);
    };
    MerkleTree.prototype.getHexProof = function (idx) {
        var result = this.getProof(idx);
        return this.bufArrToHexArr(result);
    };
    MerkleTree.prototype.getPairElement = function (idx, layer) {
        var pairIdx = idx % 2 === 0 ? idx + 1 : idx - 1;
        if (pairIdx < layer.length) {
            return layer[pairIdx];
        }
        else {
            return null;
        }
    };
    MerkleTree.prototype.bufIndexOf = function (el, arr) {
        var hash;
        // Convert element to 32 byte hash if it is not one already
        if (el.length !== 32 || !Buffer.isBuffer(el)) {
            hash = keccak160(el);
        }
        else {
            hash = el;
        }
        for (var i = 0; i < arr.length; i++) {
            if (hash.equals(arr[i])) {
                return i;
            }
        }
        return -1;
    };
    MerkleTree.prototype.bufArrToHexArr = function (arr) {
        if (arr.some(function (el) { return !Buffer.isBuffer(el); })) {
            throw new Error('Array is not an array of buffers');
        }
        return arr.map(function (el) { return '0x' + el.toString('hex'); });
    };
    return MerkleTree;
}());
exports.MerkleTree = MerkleTree;
