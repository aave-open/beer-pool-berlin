var Web3 = require('web3');
var _a = require('./merkle-tree'), MerkleTree = _a.MerkleTree, keccak160 = _a.keccak160;
var AIRDROP_COUNT = 10;
var BASE_PATH = 'https://beerlin.aave.com/#/';
var web3 = new Web3('');
var accounts = Array.from(Array(AIRDROP_COUNT).keys()).map(function (_) { return web3.eth.accounts.create(); });
var addresses = accounts.map(function (acc) { return acc.address; });
while ((addresses.length & (addresses.length - 1)) != 0) {
    addresses.push('0x1111111111111111111111111111111111111111');
}
var merkleTree = new MerkleTree(addresses);
accounts.forEach(function (acc, idx) {
    var proof = merkleTree.getHexProof(idx).join('-');
    console.log(BASE_PATH + acc.privateKey + '-' + proof);
});
console.log('Merkle Root: ', merkleTree.getHexRoot());
//
// tsc script/create.ts && node script/create.js
// https://beerlin.aave.com/#/0x5e7503be77d375b71483fc258902a9c1c16974a25d30ab43dad749a83ba1e9f4-0x7690e828ccb9043220189ba30c6e7b9723c5c8fc-0xe3d9d341343aa91e3885be722174a4f3ef698052-0x562e5c1ea9d60ff2761c5b19bbba37aee3495c58-0x3067c46b6d26c044dfe8ee27460b3e8a40c33144
// https://beerlin.aave.com/#/0x50bd4384aaace8c2d972d36a8db7ca5a684a69f2129c6cb2366abeeb009abcbd-0x7b1fcce7447c7020a2e7754d845d94dc8ab60906-0xe3d9d341343aa91e3885be722174a4f3ef698052-0x562e5c1ea9d60ff2761c5b19bbba37aee3495c58-0x3067c46b6d26c044dfe8ee27460b3e8a40c33144
// https://beerlin.aave.com/#/0x6b7faeef8e25134cd8c90a289e6b3d05a4364e0ee38d5b8e00f4d223a0c80473-0xfb1b969d385934e84c8d7e4ea6029de358326dde-0x538cd96aa1e3edd40a0e62f5718a412f846e5d1b-0x562e5c1ea9d60ff2761c5b19bbba37aee3495c58-0x3067c46b6d26c044dfe8ee27460b3e8a40c33144
// https://beerlin.aave.com/#/0xeb22aafadc37d5d6d0ac38d4c610c6bbc5a7f66d0319fdb03d965fdb1359479e-0xa3aeee25c2f3755b2ab7ad74d5ee64592149cfbf-0x538cd96aa1e3edd40a0e62f5718a412f846e5d1b-0x562e5c1ea9d60ff2761c5b19bbba37aee3495c58-0x3067c46b6d26c044dfe8ee27460b3e8a40c33144
// https://beerlin.aave.com/#/0xd85932c073f87883ad942ff2f68b1e087b1124189861357df268eba8e035d0b3-0x350a4a8c80e90eaa942cd574262e60da7c95a13c-0x34b8ca0b3aaf1ec662ca966d97fb96ad20107a70-0x11fb643a5b42db4de665618cec74704c5f776205-0x3067c46b6d26c044dfe8ee27460b3e8a40c33144
// https://beerlin.aave.com/#/0x6f11ed06c550cfb9ebc224913c4033c075fe48951ab643178d86d6f66e45a469-0x951d2194682cf6aa8ea399725aa5202ae51036ad-0x34b8ca0b3aaf1ec662ca966d97fb96ad20107a70-0x11fb643a5b42db4de665618cec74704c5f776205-0x3067c46b6d26c044dfe8ee27460b3e8a40c33144
// https://beerlin.aave.com/#/0x07561563805abc4ade4aee3953d6ae8f9ac049b8a4b21d998f42e326d97060b7-0x65d46f5c9f5251b9266dfabdf877305a12671c27-0xfed2259a29581919537871ff9d6299d5ac6ec863-0x11fb643a5b42db4de665618cec74704c5f776205-0x3067c46b6d26c044dfe8ee27460b3e8a40c33144
// https://beerlin.aave.com/#/0x8514b04a20b4c4959c978a7735103687c6945e0602608dd5197bd2335da8fab9-0x5a8d9ba0c4b3c741b65940afcf25530241ea25d3-0xfed2259a29581919537871ff9d6299d5ac6ec863-0x11fb643a5b42db4de665618cec74704c5f776205-0x3067c46b6d26c044dfe8ee27460b3e8a40c33144
// https://beerlin.aave.com/#/0x1d59896b7c69bcd622b190b4cc6feaa53b3c1eaae64eeb0f80f03ca3d6848e37-0x481c4cd2f2d313c02a454b27d78feeda9c0ee68f-0x7dbe172630a79cfd92c45a0f6078f178faddd163-0xc59683b4e5814ab0cfab35f243b5e0e2b39d1c3a-0x7347cfcd4713881546cbea0ec87048928d5c07cf
// https://beerlin.aave.com/#/0x6a48bee90dc34f7e71c9126fe43a311771fa7a62285bdba16740efdb82a3c3e0-0xb1dcdba19d270f3169b3a441b79cc5458eb2faae-0x7dbe172630a79cfd92c45a0f6078f178faddd163-0xc59683b4e5814ab0cfab35f243b5e0e2b39d1c3a-0x7347cfcd4713881546cbea0ec87048928d5c07cf
//
// Merkle Root:  0xa5d6df980707948c21d368f739077d80c9bac9bd
//
