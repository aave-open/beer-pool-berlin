# Stage 1 - the build process
FROM node:10 as build-deps
RUN apt update && apt upgrade -y
RUN apt install libudev-dev libusb-1.0-0-dev build-essential -y

WORKDIR /app

RUN npm i yarn -g
ADD ./patch.js /app/
ADD ./package.json ./yarn.lock /app/
RUN yarn

ADD ./config.json /app/
ADD ./web  /app/web
RUN yarn run build

# Stage 2 - the production environment
FROM nginx:alpine

COPY web/proxy/config.nginx /etc/nginx/nginx.conf
COPY --from=build-deps /app/web/dist/beer-pool-berlin/ /server_root/

CMD ["nginx", "-g", "daemon off;"]
